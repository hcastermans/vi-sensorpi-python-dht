#!/usr/bin/python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

import Adafruit_DHT
import web
sensor = Adafruit_DHT.DHT22

# Example using a Beaglebone Black with DHT sensor
# connected to pin P8_11.
#pin = 'P8_11'

# Example using a Raspberry Pi with DHT sensor
# connected to GPIO23.
pin = 14


urls = ("/.*", "hello")
app = web.application(urls, globals())

class hello:
    def GET(self):
      humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
      if humidity is not None and temperature is not None:
         return 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)
      else:
        return 'Failed to get reading. Try again!'

if __name__ == "__main__":
    app.run()


